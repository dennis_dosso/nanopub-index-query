package it.unipd.dei.ims.nanocit.indexing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.IndexWriter.DocStats;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import it.unipd.dei.ims.nanopub.datastructures.TrecDoc;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Offers methods to index using the methods
 * of Lucene with incremental indexing and field.
 * In particular, the considered fields are the ones of the nanopublications:
 * <p>
 * 
 * <p>
 * This class in every case lets define the list of fields to consider
 * NB: all the fields apart from the identifier are indexed as blob of text with the TextField 
 * Lucene class.
 * */
public class NanopubIndexer {

	/**Path of the directory where the index is saved*/
	private String indexPath;

	/** Path of the directory containing the documents to be indexed. We can have
	 * nested directories inside of this dir.
	 * */
	private String docDirPath;

	/** Set to true if you want to create a completely new index
	 * in the indicated directory. Otherwise the new files 
	 * will be appended if nw or updated if already present in the database*/
	private boolean overwrite;

	/** path of the stopword file with the stopwords that you want to use*/
	private String stopwordListPath;

	/** Map of the properties used in this class*/
	private Map<String, String> map;

	/** list containing the fields of the documents that we want to index.
	 * In the property file you need to specify them separated by commas
	 * and without spaces between them*/
	private String[] fields;

	/** string defining the field that we are using
	 * ad id of the documents. This field needs to be contained
	 * among the fields of the field 'fields' (nice pun, right?)
	 * e.g. DOC*/
	private String docDelimiter;
	
	
	/** This field represents the tag whose content is used to 
	 * identify one document from the other (e.g. DOCNO)
	 * */
	private String docIDentifier;

	public NanopubIndexer() {
		map = PropertiesUsefulMethods.getPropertyMap("properties/main.properties");

		indexPath = map.get("index.path");

		docDirPath = map.get("doc.dir.path");

		this.stopwordListPath = map.get("stopword.list.path");
		
		this.docDelimiter = map.get("doc.delimiter");

		overwrite = Boolean.parseBoolean(map.get("overwrite"));

		//populate the list of fields
		this.fields = map.get("fields").split(",");

		this.docIDentifier = map.get("doc.identifier");
	}


	/** Indexes the trec files contained in the 
	 * docDirPath variable
	 * 
	 * */
	public void indexNanopublications() {
		//used to keep track of the passed time
		Date start = new Date();

		//we open the object representing the index output directory
		Path out = Paths.get(indexPath);
		Directory dir = null;
		try {
			dir = FSDirectory.open(out);
		} catch (IOException e) {
			System.out.println("error in creating the index in directory: " + indexPath);
			e.printStackTrace();
		}

		//the lucene analyzer is an object containing the informations
		//about the tokenization process (e.g. stopwords and tokenizer)
		Analyzer analyzer = null;
		Analyzer standardAnalyzer = new StandardAnalyzer();

		//we give to the analyzer the stopwords to consider
		Path inStopwords = Paths.get(this.stopwordListPath);
		try(BufferedReader reader = Files.newBufferedReader(inStopwords);) {
			analyzer = new StandardAnalyzer(reader);
		} catch (IOException e) {
			System.out.println("problems with stopword file: " + stopwordListPath);
			e.printStackTrace();
			//if there are problems, we create an analyzer without stopwords
			analyzer = new StandardAnalyzer();
		}
		
		//object representing the overall set of configurations of the indexing process
		//TODO: neew to find a way to use analyzer on certain fields
		//and StandardAnalyzer to other fields
		//		IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
		IndexWriterConfig iwc = new IndexWriterConfig(standardAnalyzer);

		//set the way in which we want to create the index
		if(overwrite) {
			// Create a new index in the directory, removing any
			// previously indexed documents:
			iwc.setOpenMode(OpenMode.CREATE);
		} else {
			// Add new documents to an existing index:
			iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
		}

		iwc.setRAMBufferSizeMB(3070.0);

		//handler to the index which writes in the dir and uses the configurations specified in iwc
		IndexWriter writer;
		try {
			writer = new IndexWriter(dir, iwc);

			//index the documents
			Path in = Paths.get(this.docDirPath);
			indexDocs(writer, in);

			writer.commit();
			
			DocStats dc = writer.getDocStats();
			System.out.println("We indexed: " + dc.numDocs + " documents");
			writer.close();
			analyzer.close();
			
			Date end = new Date();
			System.out.println(end.getTime() - start.getTime() + " total milliseconds");
		} catch (IOException e) {
			System.out.println("problems with the index writer");
			e.printStackTrace();
		}
	}

	/**
	 * Indexes the given file using the given writer, or if a directory is given,
	 * recurses over files and directories found under the given directory.
	 * 
	 * @param writer Writer to the index where the given file/dir info will be stored
	 * @param path The file to index, or the directory to recurse into to find files to index
	 * @throws IOException If there is a low-level I/O error
	 */
	private void indexDocs(final IndexWriter writer, Path path) throws IOException {
		if (Files.isDirectory(path)) {
			//read all the files in the input directory
			Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
				//for every file found
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					try {
						if(!file.toFile().getName().contains(".DS_Store"))
							indexDoc(writer, file, attrs.lastModifiedTime().toMillis());
					} catch (IOException ignore) {
						// don't index files that can't be read.
					}
					return FileVisitResult.CONTINUE;
				}
			});
		} else {
			//it is a single file, not a directory
			if(!path.toFile().getName().contains(".DS_Store"))
				indexDoc(writer, path, Files.getLastModifiedTime(path).toMillis());
		}
	}

	/** Indexes a single trec file (we can have multiple documents inside the file).
	 * NB: all the fields apart from the identifier are indexed as blob of text with the TextField 
	 * Lucene class
	 * 
	 * @param writer the writer that will create the index
	 * @param file the path of the file to be added to the index
	 * 
	 * */
	private void indexDoc(IndexWriter writer, Path file, long lastModified) throws IOException {
		//open the stream to the file
		try (InputStream stream = Files.newInputStream(file)) {
			//get the path of the file to parse
			String path = file.toFile().getAbsolutePath();
			//parse all of the documents inside the file
			List<TrecDoc> list = TrecDoc.parse(path, Arrays.asList(this.fields), this.docDelimiter);

			//now we parse and the docs and insert them one at the time in the index
			for(TrecDoc d : list) {
				//create a new Lucene document
				Document doc = new Document();

				//first of all, insert the identifier field
				Field id = new StringField(this.docIDentifier, 
						d.getContentOfField(this.docIDentifier), 
						Field.Store.YES);
				doc.add(id);//add it to the document

				//now, for every remaining field
				for(String field : this.fields) {
					if(field.equals(this.docIDentifier))
						continue;//the doc id field has already been covered

					Field newField = new TextField(field, 
							d.getContentOfField(field),
							Field.Store.YES);
					doc.add(newField);
				}//covered all fields

				//adding the document to the index...
				if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
					// New index, so we just add the document (no old document can be there):
					System.out.println("adding document with id: " +  d.getContentOfField(this.docIDentifier));
					writer.addDocument(doc);
				} else {
					// Existing index (an old copy of this document may have been indexed) so 
					// we use updateDocument instead to replace the old one matching the exact 
					// path, if present:
					System.out.println("updating document with id: " + d.getContentOfField(this.docIDentifier));
					writer.updateDocument(new Term(this.docIDentifier, d.getContentOfField(this.docIDentifier)), doc);
				}
			}
		}
	}
	
	/** This is a test main. You do not need to use this, Fabio,
	 * just create an instance of this class and go with
	 * the indexNanopublications method.
	 * */
	public static void main(String[] args) {
		NanopubIndexer indexer = new NanopubIndexer();
		
		indexer.indexNanopublications();
	}

}
