package it.unipd.dei.ims.conversion;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import it.unipd.dei.ims.nanopub.datastructures.DescriptionTopic;
import it.unipd.dei.ims.nanopub.datastructures.NanopubHolder;
import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Represents a flux of operations which converts a json file in a
 * virtual document, including different strategies
 * 
 * */
public class JsonToVDConverter {

	/** Path of the single json file*/
	private String jsonPath;

	/** Path of the output directory*/
	private String outDirPath;

	/**Input directory, containing all the json files to convert*/
	private String inDirPath;

	/**Strategy in which to convert json into documents
	 * */
	public enum Strategy {PLAIN, FIELD};

	private Strategy strategy;

	Map<String, String> map;

	public JsonToVDConverter() {
		map = PropertiesUsefulMethods.getPropertyMap("properties/main.properties");

		this.jsonPath = map.get("json.path");
		this.outDirPath = map.get("out.dir.path");
		File f = new File(outDirPath);
		f.mkdirs();//create the output directory just in case
		
		this.inDirPath = map.get("in.dir.path");

		String str = map.get("strategy");
		if(str.equals("PLAIN")) {
			this.strategy = Strategy.PLAIN;
		} else if(str.equals("FIELD")) {
			this.strategy = Strategy.FIELD;
		}

	}


	/**
	 * 
	 * @param path the path of the json file to convert in document*/
	public void convertOneJsonFile(BufferedWriter writer, String path) {
		//read the json file
		JSONParser parser = new JSONParser();

		//open the file
		try (FileReader reader = new FileReader(path)) {
			//parse the file
			Object obj = parser.parse(reader);

			JSONObject jsonList = (JSONObject) obj;

			NanopubHolder holder = new NanopubHolder();


			//now we tale the elements from the json file 
			//this is strictly connected to the structure of the file
			String identifier = (String) jsonList.get("identifier");
			holder.setNanopubIdentifier(identifier);

			String creationDate = (String) jsonList.get("creationDate");
			holder.setCreationDate(creationDate);

			String rightsHolder = (String) jsonList.get("rightsHolder");
			holder.setRightsHolder(rightsHolder);

			String platform = (String) jsonList.get("platform");
			holder.setPlatform(platform);

			String version = (String) jsonList.get("version");
			holder.setVersion(version);

			Object evidenceReference = jsonList.get("evidenceReference");
			if(evidenceReference instanceof String) {
				holder.evidenceReference.add((String)evidenceReference);
			} else if(evidenceReference instanceof JSONArray) {
				JSONArray references = (JSONArray) evidenceReference;
				for(int j = 0; j < references.size(); ++j) {
					holder.evidenceReference.add((String) references.get(j));
				}
			}

			String landingpageUrl = (String) jsonList.get("landingpageUrl");
			holder.setLandingpageUrl(landingpageUrl);

			//for now, the content is an list (an array in json) with only one element, a dictionary
			JSONArray content = (JSONArray) jsonList.get("content");
			JSONObject content_ = (JSONObject) content.get(0);//only one content
			String sbj = (String) content_.get("subject");

			//we create the topic forming the content
			DescriptionTopic content__ = new DescriptionTopic();
			content__.setSubject(sbj);
			//take now the assertion of the content
			JSONArray assertions = (JSONArray) content_.get("assertion");
			content__.populateAssertions(assertions);
			holder.setContent(content__);


			//now creators - unbounded
			JSONArray creator = (JSONArray) jsonList.get("creator");
			holder.populateCreators(creator);

			//now contributors - unbounded
			JSONArray contributor = (JSONArray) jsonList.get("contributor");
			holder.populateContributor(contributor);

			//now evidence Author
			JSONArray evidenceAuthors = (JSONArray) jsonList.get("evidenceAuthor");
			holder.populateEvidenceAuthors(evidenceAuthors);

			//now that we have all the information in the json, we can write the file
			this.writeOneFile(holder, writer);



		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

	public void writeOneFile(NanopubHolder holder, BufferedWriter writer) {
		if(this.strategy == Strategy.PLAIN) {
			this.writeOnePlainFile(holder, writer);
		} else if(this.strategy == Strategy.FIELD) {
			this.writeOneFieldedFile(holder, writer);
		}
	}


	/**We write one document with plain text, without any field
	 * */
	public void writeOnePlainFile(NanopubHolder holder, BufferedWriter writer) {
		String document = "<DOC>\n";
		//add the id
		document += "<DOCNO>" + holder.getNanopubIdentifier() + "</DOCNO>\n";

		//the field content always contains everything else
		document += "<TEXT>" + 
				holder.getCreationDate() + "\n" +
				holder.getRightsHolder() + "\n" + 
				holder.getPlatform() + "\n" +
				holder.getVersion() + "\n" +
				holder.getEvidenceReference() + "\n" +
				holder.getLandingpageUrl() + "\n";

		document += holder.printContent() +
				holder.printCreator()  +
				holder.printContributor() +
				holder.printEvidenceAuthor();

		document += "</TEXT>\n</DOC>\n";

		//and now we print it
		try {
			writer.write(document);
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	private void writeOneFieldedFile(NanopubHolder holder, BufferedWriter writer) {
		String document = "<DOC>\n";
		//add the id
		document += "<DOCNO>" + holder.getNanopubIdentifier() + "</DOCNO>\n";

		//print the content
		document += "<CONTENT>" + holder.printContent() + "\n</CONTENT>\n";
		//print the creator
		document += "<CREATOR>" + holder.printCreator() + "</CREATOR>\n";
		//print the contributor
		document += "<CONTRIBUTOR>" + holder.printContributor() + "</CONTRIBUTOR>\n";
		//print the evidence author
		document += "<EVIDENCEAUTHOR>" + holder.printEvidenceAuthor() + "</EVIDENCEAUTHOR>\n";

		//content field (the rest)
		document += "<TEXT>" + 
				holder.getCreationDate() + " " +
				holder.getRightsHolder() + " " + 
				holder.getPlatform() + " " +
				holder.getVersion() + " " +
				holder.getEvidenceReference() + " " +
				holder.getLandingpageUrl() + "\n";

		document += holder.printContent() +
				holder.printCreator() + 
				holder.printContributor() +
				holder.printEvidenceAuthor();

		document += "</TEXT>\n";

		//end of the document
		document += "</DOC>\n";

		//and now we print it
		try {
			writer.write(document);
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void convertOneJsonFile(BufferedWriter writer) {
		this.convertOneJsonFile(writer, this.jsonPath);
	}


	public void convertAllJSONInDirectory() throws IOException {
		List<String> listOfFiles = PathUsefulMethods.getListOfFiles(this.inDirPath);
		//counter in order to not create too big trec files 
		int counter = 0;
		int fileCounter = 0;
		BufferedWriter writer = null;
		for(String s: listOfFiles) {
			if(counter % 1024 == 0) {
				if(writer != null) {
					//close the previous stream
					writer.close();
				}
				//create a new stream
				String outputFile = this.outDirPath + "/" + fileCounter + ".trec";
				fileCounter++;
				//open the stream
				writer = new BufferedWriter(new FileWriter(outputFile, false));
				counter = 0;
			}//end if
			//convert this file
			this.convertOneJsonFile(writer, s);
			counter++;
		}
	}


	/** Test main during the debug. Do not care about this
	 * (talking to you, Fabio)*/
	public static void main(String[] args) {
		JsonToVDConverter execution = new JsonToVDConverter();

		//debug
		try {
			execution.convertAllJSONInDirectory();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("done");
	}
}
