package it.unipd.dei.ims.conversion;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import it.unipd.dei.ims.nanopub.datastructures.Content;
import it.unipd.dei.ims.nanopub.datastructures.DescriptionTopic;
import it.unipd.dei.ims.nanopub.datastructures.Entity;
import it.unipd.dei.ims.nanopub.datastructures.EvidenceReference;
import it.unipd.dei.ims.nanopub.datastructures.ExtendedNanopubHolder;
import it.unipd.dei.ims.nanopub.datastructures.ExtendedPerson;
import it.unipd.dei.ims.nanopub.datastructures.NanopubHolder;

/** Class to convert files from json format to trec.
 * An improvement on the father class since
 * here we are considering more fields.
 * 
 * Go to the father class JsonTOVDConverter
 * for all the documentation.
 * 
 * */
public class JsonToTRECConverter extends JsonToVDConverter {

	public JsonToTRECConverter() {
		super();
	}

	@Override
	public void convertOneJsonFile(BufferedWriter writer, String path) {
		//read the json file
		JSONParser parser = new JSONParser();

		//open the file
		try (FileReader reader = new FileReader(path)) {
			//parse the file
			Object obj = parser.parse(reader);
			
			JSONObject jsonList = (JSONObject) obj;
			ExtendedNanopubHolder holder = new ExtendedNanopubHolder();


			//now we tale the elements from the json file 
			//this is strictly connected to the structure of the file
			
			//identifier
			String identifier = (String) jsonList.get("identifier");
			holder.setIdentifier(identifier);

			Object ob = jsonList.get("evidenceReferences");
			//the references are actually an array of elements
			JSONArray evidenceReferences = (JSONArray) ob;
			//for every reference
			for(int i = 0; i < evidenceReferences.size(); ++i) {
				//take the reference from the JSON
				JSONObject reference = (JSONObject) evidenceReferences.get(i);
				//create the evidence reference object and populate it
				EvidenceReference ed = new EvidenceReference();
				ed.setNameType((String)reference.get("name_type"));
				ed.setAbstractText((String) reference.get("abstract_text"));
				ed.setType((String)reference.get("type"));
				ed.setTitle((String)reference.get("title"));
				ed.setIdEvid((String)reference.get("ed_evid"));
				ed.setContent((String)reference.get("content"));
				//the authors are another array (yeah, seriously)
				JSONArray authorsArray = (JSONArray)reference.get("authors");
				for(int j = 0; j < authorsArray.size(); ++j) {
					JSONObject author = (JSONObject) authorsArray.get(j);
					//prepare the object author and populate it
					ExtendedPerson auth = new ExtendedPerson();
					auth.setTypeId((String) author.get("type_id"));
					auth.setName((String) author.get("name"));
					auth.setTypeAgent((String) author.get("type_agent"));
					auth.setFamilyName((String) author.get("family_name"));
					auth.setIdAgent((String) author.get("id_agent"));
					
					//now that we populated the thing, we can add it to the list of the reference
					ed.addToAuthors(auth);
				}
				//we have completed the evidence reference, we add it to the holder
				holder.addToEvidenceReferenes(ed);
			}
			
			holder.setGenerationComment((String)jsonList.get("generationComment"));
			holder.setRightsHolderId((String)jsonList.get("rights_holder_id"));
			
			//now we the creators, even more arrays
			JSONArray creators = (JSONArray) jsonList.get("creators");
			for(int i = 0; i < creators.size(); ++i) {
				JSONObject creator = (JSONObject) creators.get(i);
				//prepare the object creator, a Person
				ExtendedPerson auth = new ExtendedPerson();
				auth.setTypeId((String) creator.get("type_id"));
				auth.setName((String) creator.get("name"));
				auth.setTypeAgent((String) creator.get("type_agent"));
				auth.setFamilyName((String) creator.get("family_name"));
				auth.setIdAgent((String) creator.get("id_agent"));
				
				holder.addToCreators(auth);
			}
			
			holder.setLandingPage((String)jsonList.get("landing_page"));
			holder.setCreationDate((String)jsonList.get("creation_date"));
			holder.setPlatform((String)jsonList.get("platform"));
			
			//now for the content
			JSONObject content = (JSONObject) jsonList.get("content");
			Content cont = new Content();
			cont.setIdAssertion((String)content.get("id_assertion"));
			cont.setSubject((String)content.get("subject"));
			cont.setDescription((String)content.get("description"));
			JSONArray entities = (JSONArray) content.get("entities");
			for(int i = 0; i < entities.size(); ++i) {
				JSONObject entity = (JSONObject) entities.get(i);
				
				Entity ent = new Entity();
				ent.setIdEntity((String)entity.get("id_entity"));
				ent.setEntityType((String)entity.get("entity_type"));
				ent.setName((String)entity.get("name"));
				ent.setType((String)entity.get("type"));
				ent.setDescription((String)entity.get("description"));
				
				cont.addToEntities(ent);
			}
			holder.setContent(cont);
			
			holder.setUrlNp((String)jsonList.get("url_np"));
			holder.setGeneratedBy((String)jsonList.get("denerated_by"));
			holder.setPlatformVersion((String)jsonList.get("platform_version"));
			holder.setRights((String)jsonList.get("rights"));
			holder.setPlatformId((String)jsonList.get("platform_id"));
			
			JSONArray collaborators = (JSONArray) jsonList.get("collaborators");
			for(int i = 0; i < collaborators.size(); ++i) {
				JSONObject collaborator = (JSONObject) collaborators.get(i);
				
				//prepare the new object collaborator, a Person
				ExtendedPerson auth = new ExtendedPerson();
				auth.setTypeId((String) collaborator.get("type_id"));
				auth.setName((String) collaborator.get("name"));
				auth.setTypeAgent((String) collaborator.get("type_agent"));
				auth.setFamilyName((String) collaborator.get("family_name"));
				auth.setIdAgent((String) collaborator.get("id_agent"));
				
				holder.addToCollaborators(auth);
			}
			
			holder.setRightsHolder((String)jsonList.get("rights_holder"));
			
			//now that we have all the information in the json, we can write the file
			this.writeOneTrecFile(holder, writer);



		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	/** Writes one file in the trec format using the information
	 * contained in the holder
	 * */
	private void writeOneTrecFile(ExtendedNanopubHolder holder, BufferedWriter writer) {
		String document = "<DOC>\n";
		//add the id
		document += "<DOCNO>" + holder.getIdentifier() + "</DOCNO>\n";
		
		
		String body = "";
		//fields from the evidence references
		//(abstract, title, authors)
		String abstract_ = "", title = "", authors = "", name_type = "", type="", content = "";
		List<EvidenceReference> evidenceReferences = holder.getEvidenceReferences();
		for(int i = 0; i < evidenceReferences.size(); ++i) {
			EvidenceReference e = evidenceReferences.get(i);
			
			//build the fields for every reference
			name_type += e.getNameType() + " ";
			type += e.getType() + " ";
			content += e.getContent() + " ";
			abstract_ += e.getAbstractText() + " ";
			title += e.getTitle() + " ";
			authors += e.getAuthorsAsOneString() + " ";
		}
		//we have merged all the fields of the references, now we print them
		document += "<NAMETYPE>\n" + name_type + "\n</NAMETYPE>\n";
		body += name_type + " ";
		
		document += "<TYPE>\n" + type + "\n</TYPE>\n";
		body += type + " ";
		
		document += "<REFERENCECONTENT>\n" + content + "\n</REFERENCECONTENT>\n";
		body += content + " ";
		
		document += "<ABSTRACT>\n" + abstract_ + "\n</ABSTRACT>\n";
		body += abstract_ + " ";
		
		document += "<TITLE>\n" + title + "\n</TITLE>\n";
		body += title + " ";
		
		document += "<AUTHORS>\n" + authors + "\n</AUTHORS>\n";
		body += authors + " ";
		//end of references
		
		document += "<GENERATIONCOMMENT>\n" + holder.getGenerationComment() + "\n</GENERATIONCOMMENT>\n";
		body += holder.getGenerationComment() + " ";
		
		document += "<CREATORS>\n" + holder.getCreatorsAsOneString() + "\n</CREATORS>\n";
		body += holder.getCreatorsAsOneString() + " ";
		
		document += "<PLATFORM>\n" + holder.getPlatform() + "\n</PLATFORM>\n";
		body += holder.getPlatform() + " ";
		
		//two of the most important fields
		document += "<SUBJECT>\n" + holder.getCont().getSubject() + "\n</SUBJECT>\n";
		body += holder.getCont().getSubject() + " ";
		
		document += "<DESCRIPTION>\n" + holder.getCont().getDescription() + "\n</DESCRIPTION>\n";
		body += holder.getCont().getDescription() + " ";
		
		document += "<COLLABORATORS>\n" + holder.getCollaboratorsAsOneString() + "\n</COLLABORATORS>\n";
		body += holder.getCollaboratorsAsOneString() + " ";
		
		//all together passionately
		document += "<BODY>\n" + body + "\n</BODY>\n";
		
		//end of the document
		document += "</DOC>\n";

		//and now we print it
		try {
			writer.write(document);
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/** Test main during the debug. Do not care about this
	 * (talking to you, Fabio)*/
	public static void main(String[] args) {
		JsonToTRECConverter execution = new JsonToTRECConverter();

		//debug
		try {
			execution.convertAllJSONInDirectory();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("done");
	}
}
