package it.unipd.dei.ims.nanopub.datastructures;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/** A class representing a trec document. It also includes a method
 * to parse the documents and automatically fill the fields.
 * 
 * */
public class TrecDoc {
	
	/** a map representing the fields of this document. Every key 
	 * is the name of the field (e.g. DOCNO, TEXT, CONTENT etc.).
	 * Every value corresponds to the content of the corresponding field
	 * */
	Map<String, String> fieldsContent;
	
	/** Path of the file*/
	private String path;
	
	private List<String> fields;
	
	/** tag describing the starting and ending point of a document
	 * inside a trec file
	 * */
	private String docDelimiter;
	
	/**Name of the field used as ID of the documents*/
	private String docId;
	
	public TrecDoc() {
		fieldsContent = new HashMap<String, String>();
		fields = new ArrayList<String>();
	}
	
	
	/** Set the fields that we are going to parse in this document*/
	public void setFields(List<String> f) {
		fields = f;
	}
	
	/** Add one field to the list of considered fields*/
	public void addField(String f) {
		fields.add(f);
	}
	
	public void removeField(String f) {
		fields.remove(f);
	}
	
	/** Adds, at the specified field, the content provided in the 
	 * text param.
	 * If some content was already present it is overwritten.
	 * 
	 * */
	public void setFieldContent(String field, String text) {
		this.fieldsContent.put(field, text);
	}
	
	public String getContentOfField(String field) {
		return this.fieldsContent.get(field);
	}
	
	
	/** parses the document whose path is given as parameter
	 * considering the provided fields. It divides the 
	 * documents in the file using the docDelimiter parameter.
	 * Returns a list of TrecDoc objects representing the documents.
	 *
	 * @param path the path of the trec file to parse
	 * @param fields a list containing the fields we want to consider
	 * @param docDelimiter string with the tag which divides the documents inside the file
	 * 
	 * @return a list of TrecDoc (trec documents) obtained from the file
	 * 
	 * */
	public static List<TrecDoc> parse(String path, List<String> fields, String docDelimiter) {
		System.out.println("parsing trec file:\n " + path + "\n");
		
		File input = new File(path);
		List<TrecDoc> trecDocs = new ArrayList<TrecDoc>();
		
		try {
			//first: parse the whole file
			Document doc = Jsoup.parse(input, "UTF-8");
			//let us divide the documents inside the file
			Elements documents = doc.getElementsByTag(docDelimiter);
			//for every document inside the file
			for(Element document : documents) {
				TrecDoc trecDoc = new TrecDoc();
				
				//now, for every tag
				for(String tag : fields) {
					//get the content of the tag
					String field = document.getElementsByTag(tag).html();
					//insert in the corresponding field of this document
					trecDoc.setFieldContent(tag, field);
				}//end of the tags
				//add the document to the list
				trecDocs.add(trecDoc);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return trecDocs;
	}
	
	
	
	/** Test main, used during development. Do not use
	 * */
	public static void main(String[] args) {
		String docPath = "/Users/dennisdosso/Documents/Ricerca/nanocit/docs/1and2.trec";
		
		
		List<String> fields = new ArrayList<String>();
		fields.add("TEXT");
		fields.add("DOCNO");

		List<TrecDoc> list = TrecDoc.parse(docPath, fields, "DOC");
		
		System.out.println("done");
		
	}

	public Map<String, String> getFields() {
		return fieldsContent;
	}

	public void setFields(Map<String, String> fields) {
		this.fieldsContent = fields;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}


	public String getDocDelimiter() {
		return docDelimiter;
	}


	public void setDocDelimiter(String docDelimiter) {
		this.docDelimiter = docDelimiter;
	}


	public Map<String, String> getFieldsContent() {
		return fieldsContent;
	}


	public void setFieldsContent(Map<String, String> fieldsContent) {
		this.fieldsContent = fieldsContent;
	}


	public String getDocId() {
		return docId;
	}


	public void setDocId(String docId) {
		this.docId = docId;
	}
	
	public String toString() {
		String s = "";
		for(String f : this.fieldsContent.keySet()) {
			String fieldContent = fieldsContent.get(f);
			int limit = fieldContent.length();
			if(limit > 9)
				fieldContent = fieldContent.substring(0, 9);
			s += f + " " + fieldContent + " ";
		}
		return "fields: " + s;
	}
	
	
	
	
	
}
