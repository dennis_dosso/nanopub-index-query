package it.unipd.dei.ims.nanopub.datastructures;

/**Represents a person in an nanopublication with a 
 * bigger number of fields. 
 * */
public class ExtendedPerson {
	private String typeId;
	private String name;
	private String familyName;
	private String typeAgent;
	private String idAgent;
	
	
	
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getTypeAgent() {
		return typeAgent;
	}
	public void setTypeAgent(String typeAgent) {
		this.typeAgent = typeAgent;
	}
	public String getIdAgent() {
		return idAgent;
	}
	public void setIdAgent(String idAgent) {
		this.idAgent = idAgent;
	}
}
