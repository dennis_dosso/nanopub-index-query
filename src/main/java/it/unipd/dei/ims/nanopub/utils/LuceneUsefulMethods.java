package it.unipd.dei.ims.nanopub.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

public class LuceneUsefulMethods {

	
	/** builds an analyzer with a provided stopword list given in the
	 * path of a file*/
	public static Analyzer getAnalyzer(String stopWordListPath) {
		Analyzer analyzer = null;

		//we give to the analyzer the stopwords to consider
		Path inStopwords = Paths.get(stopWordListPath);
		try(BufferedReader reader = Files.newBufferedReader(inStopwords);) {
			analyzer = new StandardAnalyzer(reader);
		} catch (IOException e) {
			System.out.println("problems with stopword file: " + stopWordListPath);
			e.printStackTrace();
			//if there are problems, we create an analyzer without stopwords
			analyzer = new StandardAnalyzer();
		}
		return analyzer;
	}
}
