package it.unipd.dei.ims.nanopub.query;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;

import it.unipd.dei.ims.nanopub.utils.LuceneUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Exposes methods to query trec indexes using Lucene
 * in the context of the Nanopub project
 * <p>
 * One way to use this class is to instantiate one object and then
 * call the performQueries algorithm if you have all 
 * your queries in a property file. The method will 
 * query only one field of each document.
 * <p>
 * Alternatively (and probably the thing
 * you are looking for), 
 * you can create one instance and call the 
 * setup() method, which will use the propety file to 
 * open the index, set the IndexSearcher on the index,
 * set the model to BM25 and set the Analyzer
 * to StandardAnalyzer (no stop words, no stemming).
 * <p>
 * Then call the performOneQuery method where you can specify your query
 * and the field where you want to focus. It will return 
 * your list of answers.
 * <p>
 * At the end, remember to close everything with the shutDown method.
 * */
public class NanopubQueryManager {

	/** path of the index*/
	private String indexPath;

	/** Path of the file with the queries*/
	private String queryFilePath;

	/** model to use to interrogate*/
	private String model;

	/** field to query*/
	private String field;


	/** string defining the field that we are using
	 * ad id of the documents. This field needs to be contained
	 * among the fields of the field 'fields' (nice pun, right?)*/
	private String docDelimiter;

	private String docIdentifier;

	private String stopWordListPath;

	/** Map of the properties used in this class*/
	private Map<String, String> map;
	
	private IndexReader reader;
	
	private IndexSearcher searcher;
	
	private Analyzer analyzer;

	public NanopubQueryManager() {
		map = PropertiesUsefulMethods.getPropertyMap("properties/main.properties");

		indexPath = map.get("index.path");

		field = map.get("field");

		this.queryFilePath = map.get("query.file.path");

		this.model = map.get("model");

		this.stopWordListPath = map.get("stopword.list.path");

		this.docDelimiter = map.get("doc.delimiter");

		this.docIdentifier = map.get("doc.identifier");
	}

	/** Method to perform all of the queries in the file
	 * contained in the queryFilePath field. 
	 * If you want only one query put only one query in the file.
	 * The structure of the file should be:
	 * <p>
	 * 1=[query 1]
	 * <p>
	 * 2=[query 2]
	 * <p>
	 * etc*/
	private void performQueries() {
		//read the queries from the property file
		Map<String, String> queryMap = PropertiesUsefulMethods.getPropertyMap(this.queryFilePath);


		//open the index
		Path in = Paths.get(indexPath);
		IndexReader reader = null;
		try {
			reader = DirectoryReader.open(FSDirectory.open(in));
		} catch (IOException e) {
			e.printStackTrace();
		}

		//open the searcher over the index
		IndexSearcher searcher = new IndexSearcher(reader);

		//set the retrieval model of the searcher
		//TODO implement the possibility to choose your model
		Similarity simfn = new BM25Similarity();
		searcher.setSimilarity(simfn);

		//set the analyzer with stopwords 
		//TODO find a way to interrogate with different analyzers
		Analyzer analyzer = LuceneUsefulMethods.getAnalyzer(this.stopWordListPath);
		analyzer = new StandardAnalyzer();

		for(Entry<String, String> entry : queryMap.entrySet()) {
			String queryId = entry.getKey();
			String query = entry.getValue();

			this.performOneQuery(queryId, query, analyzer, searcher);
		}

		try {
			analyzer.close();
			reader.close();
		} catch (IOException e) {
			System.out.println("problems closing the index");
			e.printStackTrace();
		}
	}

	/** this method is used to open the index and prepare
	 * all things related to querying it.
	 * It can be used in order to open the index and then 
	 * keep rolling with it until you need to shut down,
	 * so you won't need to open and close it again and again.
	 * <p>
	 * NB: when you are done, call shutDown
	 * */
	public void setup() {
		//open the index
		Path in = Paths.get(indexPath);
		this.reader = null;
		try {
			this.reader = DirectoryReader.open(FSDirectory.open(in));
		} catch (IOException e) {
			e.printStackTrace();
		}

		//open the searcher over the index
		this.searcher = new IndexSearcher(reader);

		//set the BM25 as model. May need some other model in the future
		Similarity simfn = new BM25Similarity();
		searcher.setSimilarity(simfn);

		//set the analyzer with stopwords 
//		this.analyzer = LuceneUsefulMethods.getAnalyzer(this.stopWordListPath);
		this.analyzer = new StandardAnalyzer();
	}
	
	
	/** closes all the things that were opened by the setup method
	 * @throws IOException When something goes wrong trying to close the reader*/
	public void shutDown() throws IOException {
		this.reader.close();
		this.analyzer.close();
	}

	public List<String> performOneQuery(String query) {
		return this.performOneQuery("0", query, this.analyzer, this.searcher);
	}
	
	/** Performs one single query on the specified field.
	 * You should make sure to have called the setup() method
	 * if you are using this method as a stand-alone.*/
	public List<String> performOneQuery(String query, String f) {
		this.field = f;
		return this.performOneQuery("0", query, this.analyzer, this.searcher);
	}

	/**Performs one query. Only one field is interrogate,
	 * and that is the this.field field. If you want to interrogate
	 * different fields, call this method many times 
	 * changing the field.
	 * 
	 * @return a list of the docno of the documents returned by the query
	 * on the index. The list is empty if no answers were produced*/
	public List<String> performOneQuery(String queryId, String query, Analyzer analyzer, IndexSearcher searcher) {
		List<String> answerList = new ArrayList<String>();
		
		//parse the query
		QueryParser parser = new QueryParser(this.field, analyzer);
		Query q = null;
		try {
			q = parser.parse(query);
		} catch (ParseException e) {
			System.out.println("some parsing issue with query " + query);
			e.printStackTrace();
		}

		//perform the query
		TopDocs td = null;
		try {
			td = searcher.search(q, 1000);
		} catch (IOException e) {
			e.printStackTrace();
		}
		assert(td!=null);

		ScoreDoc[] hits = td.scoreDocs;
		int rank = 0;
		if(hits.length == 0) {//query with no answers
			System.out.println(queryId + " Q0 " 
					+	" NAN " 
					+   "NAN  NAN " 
					+ " bm25 (no answers for query " + queryId + ")");
		}
		for(ScoreDoc sd : hits) {
			Document hitDoc;
			try {
				hitDoc = searcher.doc(sd.doc);

				//print answer in trec_eval fashion
				System.out.println(queryId + " Q0 " + 
						hitDoc.get(this.docIdentifier) + " " 
						+ rank + " " 
						+ sd.score + " bm25");
				rank ++;
				answerList.add(hitDoc.get(this.docIdentifier));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}//end for
		
		return answerList;

	}

	/**Test main that was used in debug
	 * To use the class in your project create an instance
	 * and query with the performQueries method.*/
	public static void main(String[] args) {
		NanopubQueryManager execution = new NanopubQueryManager();

		execution.performQueries();
	}


}
