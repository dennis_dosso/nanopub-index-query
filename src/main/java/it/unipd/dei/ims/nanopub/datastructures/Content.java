package it.unipd.dei.ims.nanopub.datastructures;

import java.util.ArrayList;
import java.util.List;

/**Object representing one content of a nanopublication
 * */
public class Content {
	private String idAssertion;
	private String subject;
	private String description;
	
	private List<Entity> entities;
	
	public Content() {
		this.entities = new ArrayList<Entity>();
	}

	public void addToEntities(Entity ent) {
		entities.add(ent);
	}
	
	
	public String getIdAssertion() {
		return idAssertion;
	}

	public void setIdAssertion(String idAssertion) {
		this.idAssertion = idAssertion;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Entity> getEntities() {
		return entities;
	}

	public void setEntities(List<Entity> entities) {
		this.entities = entities;
	}
	
}
