package it.unipd.dei.ims.nanopub.datastructures;

/**Representation of a person in json
 * <p>
 * Term element: given_name
		URI: http://xmlns.com/foaf/0.1/givenname
		Label: Given_name
		Minimum: 0
		Maximum: 1
		Type of Value: literal
		Definition: The given name of some person.
	Term element: family_name
		URI: http://xmlns.com/foaf/0.1/family_name
		Label: FamilyName
		Minimum: 0
		Maximum: 1
		Type of Value: literal
		Definition: The family name of some person.
	Term element: personID
		URI: http://purl.org/dc/terms/identifier
		Label: personID
		Minimum: 0
		Maximum: 1
		Type of Value: literal
		Definition: An identifier (ORDCID, Researcher ID) that distinguishes a person.
		*/
public class Person {

	public String givenName;
	public String familyName;
	public String personID;
	
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getPersonID() {
		return personID;
	}
	public void setPersonID(String personID) {
		this.personID = personID;
	}
	
}
