package it.unipd.dei.ims.nanopub.datastructures;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;

/**Term element: descriptionTopic
	Term element: subject
		URI: http://purl.org/dc/elements/1.1/subject
		Label: Subject
		Minimum: 0
		Maximum: 1
		Type of Value: literal
		Definition: The given name of some person.
	Term element: assertion
		URI: http://purl.org/dc/elements/1.1/description
		Label: Assertion
		Minimum: 1
		Maximum: unbounded
		Type of Value: literal
		Definition: An assertion content.*/
public class DescriptionTopic {
	public String subject;
	public List<String> assertions;
	
	public DescriptionTopic() {
		this.assertions = new ArrayList<String>();
	}
	
	/** Supposing that the input array is a json array of strings,
	 * it populates the list of assertion of this description topic*/
	public void populateAssertions(JSONArray array) {
		for(int j = 0; j < array.size(); ++j) {
			String a = (String) array.get(j);
			assertions.add(a);
		}
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public List<String> getAssertions() {
		return assertions;
	}

	public void setAssertions(List<String> assertions) {
		this.assertions = assertions;
	}
}
