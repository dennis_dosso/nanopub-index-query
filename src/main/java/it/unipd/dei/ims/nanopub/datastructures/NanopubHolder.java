package it.unipd.dei.ims.nanopub.datastructures;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**Object representing the fields of a nanocitation
 * in order to easily (?) handle them.
 * 
 * */
public class NanopubHolder {

	/**Term element: nanopubIdentifier
	URI: http://purl.org/dc/terms/identifier
	Label: NanopubIdentifier
	Minimum: 1
	Maximum: 1
	Type of Value: literal
	Definition: The URI that identifies nanopublication.*/
	public String nanopubIdentifier;

	/** Term element: creator
	URI: http://purl.org/dc/terms/creator
	Label: Creator
	Minimum: 1
	Maximum: unbounded
	Type of Value: non-literal
	Defined as: agent
	Definition: An agent (a person, a organisation or a software agent) primarily responsible for creating the nanopublication.*/
	public List<Agent> creators;

	/**Term element: contributor
	URI: http://purl.org/dc/terms/contributor
	Label: Contributor
	Minimum: 0
	Maximum: unbounded
	Type of Value: non-literal
	Defined as: agent
	Definition: An agent (a person, an organisation or a service) responsible for making contributions to the creation of the nanopublication.*/
	public List<Agent> contributors;

	/**Term element: creationDate
	URI: http://purl.org/dc/terms/date
	Label: CreationDate
	Minimum: 0
	Maximum: 1
	Type of Value: literal
	Syntax Encoding Scheme URI = http://purl.org/dc/terms/W3CDTF
	Definition: Date of creation of the nanopublication.*/
	public String creationDate;

	/**Term element: rightsHolder
	URI: http://purl.org/dc/terms/rightsHolder
	Label: RightsHolder
	Minimum: 0
	Maximum: 1
	Type of Value: literal
	Definition: A person or organisation owning or managing rights over the nanopublication.*/
	public String rightsHolder;


	/**Term element: content
	Label: Content
	Minimum: 1
	Maximum: 1
	Type of Value: non-literal
	Defined as: descriptionTopic
	Definition: The topic of the nanopublication and a description of the assertion*/
	public DescriptionTopic content;

	/**Term element: platform
	URI: http://purl.org/dc/elements/1.1/publisher
	Label: Platform
	Minimum: 0
	Maximum: 1
	Type of Value: literal
	Definition: An entity responsible for making the nanopublication available.
	 */
	public String platform;

	/**Term element: version
	URI: http://purl.org/dc/terms/hasVersion
	Label: Version
	Minimum: 0
	Maximum: 1
	Type of Value: literal
	Definition: The version of the platform.*/
	public String version;


	/**Term element: evidenceReference
	URI: http://purl.org/dc/terms/references
	Label: EvidenceReference
	Minimum: 0
	Maximum: unbounded
	Type of Value: literal
	Definition: A related resource that is referenced as an assertion evidence.*/
	public List<String> evidenceReference;


	/** Term element: evidenceAuthor
	URI: http://purl.org/dc/terms/creator
	Label: EvidenceAuthor
	Minimum: 0
	Maximum: unbounded
	Type of Value: non-literal
	Defined as: person
	Definition: The person responsible authoring the evidence paper from which the assertion was extracted.*/
	public List<Person> evidenceAuthors;


	/** Term element: landingpageUrl
	URI: http://purl.org/dc/terms/identifier
	Label: LandingPageURL
	Minimum: 1
	Maximum: 1
	Type of Value: literal
	Definition: An identifier (URL) of the landingpage related to the nanopublication.
	 */
	public String landingpageUrl;


	public NanopubHolder() {
		content = new DescriptionTopic();
		evidenceReference = new ArrayList<String>();
		evidenceAuthors = new ArrayList<Person>();
		contributors = new ArrayList<Agent>();
		creators = new ArrayList<Agent>();
	}

	/** Given a json array containing objects of tipe creator,
	 * populates the creators field of this holder
	 * */
	public void populateCreators(JSONArray creators_) {
		for(int j = 0; j < creators_.size(); ++j) {
			JSONObject creator = (JSONObject) creators_.get(j);
			//new agent representing the creator
			Agent c = new Agent();
			//new person inside the agent
			Person pers = new Person();
			JSONObject pers_ = (JSONObject) creator.get("pers");
			pers.setGivenName((String)pers_.get("given_name"));
			pers.setFamilyName((String)pers_.get("family_name"));
			pers.setPersonID((String) pers_.get("personID"));
			c.setPers(pers);
			c.setOrg((String) creator.get("org"));
			//now add the creator
			this.creators.add(c);
		}
	}

	public void populateContributor(JSONArray contributors_) {
		for(int j = 0; j < contributors_.size(); ++j) {
			//get the contributor
			JSONObject contributor = (JSONObject) contributors_.get(j);
			//new agent representing the creator
			Agent c = new Agent();
			//new person inside the agent
			Person pers = new Person();
			JSONObject pers_ = (JSONObject) contributor.get("pers");
			pers.setGivenName((String)pers_.get("given_name"));
			pers.setFamilyName((String)pers_.get("family_name"));
			pers.setPersonID((String) pers_.get("personID"));
			c.setPers(pers);
			c.setOrg((String) contributor.get("org"));
			//now add the creator
			this.contributors.add(c);
		}
	}

	public void populateEvidenceAuthors(JSONArray authors_) {
		for(int j = 0; j < authors_.size(); ++j) {
			//get the contributor
			JSONObject contributor = (JSONObject) authors_.get(j);

			//new person representing the author
			Person pers = new Person();
			//take the information about the person
			pers.setGivenName((String)contributor.get("given_name"));
			pers.setFamilyName((String)contributor.get("family_name"));
			pers.setPersonID((String) contributor.get("personID"));
			//now add the creator
			this.evidenceAuthors.add(pers);
		}
	}

	public String printContent() {
		String r = "";
		r += this.content.getSubject();
		r += " " + this.content.getAssertions();

		return r;
	}


	public String printCreator() {
		String r = "";

		for(int j = 0; j < this.creators.size(); ++j) {
			Agent smith = creators.get(j);//get the reference please

			Person p = smith.getPers();
			String givenName = p.getGivenName();
			if(givenName != null && !givenName.equals("null")) {
				r += " " + givenName;
			}
			if(p.getFamilyName() != null && !p.getFamilyName().equals("null")) {
				r += " " + p.getFamilyName();
			}
			String personId = p.getPersonID();
			if(personId != null && !personId.equals("null")) {
				r += " " + personId;
			}

			String org = smith.getOrg();
			if(org != null && !org.equals("null"))
				r += " " + org;

			r += "\n";
		}
		return r;
	}

	public String printContributor() {
		String r = "";

		for(int j = 0; j < this.contributors.size(); ++j) {
			Agent smith = contributors.get(j);

			Person p = smith.getPers();
			String givenName = p.getGivenName();
			if(givenName != null && !givenName.equals("null")) {
				r += " " + givenName;
			}
			if(p.getFamilyName() != null && !p.getFamilyName().equals("null")) {
				r += " " + p.getFamilyName();
			}
			String personId = p.getPersonID();
			if(personId != null && !personId.equals("null")) {
				r += " " + personId;
			}

			String org = smith.getOrg();
			if(org != null && !org.equals("null"))
				r += " " + org;

			r += "\n";
		}
		return r;
	}

	public String printEvidenceAuthor() {
		String r = "";

		for(int j = 0; j < this.evidenceAuthors.size(); ++j) {
			Person p = evidenceAuthors.get(j);

			String givenName = p.getGivenName();
			if(givenName != null && !givenName.equals("null")) {
				r += " " + givenName;
			}
			if(p.getFamilyName() != null && !p.getFamilyName().equals("null")) {
				r += " " + p.getFamilyName();
			}
			String personId = p.getPersonID();
			if(personId != null && !personId.equals("null")) {
				r += " " + personId;
			}
			r += "\n";
		}
		return r;
	}

	public String getNanopubIdentifier() {
		return nanopubIdentifier;
	}


	public void setNanopubIdentifier(String nanopubIdentifier) {
		this.nanopubIdentifier = nanopubIdentifier;
	}


	public List<Agent> getCreators() {
		return creators;
	}


	public void setCreators(List<Agent> creators) {
		this.creators = creators;
	}


	public List<Agent> getContributors() {
		return contributors;
	}


	public void setContributors(List<Agent> contributors) {
		this.contributors = contributors;
	}


	public String getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}


	public String getRightsHolder() {
		return rightsHolder;
	}


	public void setRightsHolder(String rightsHolder) {
		this.rightsHolder = rightsHolder;
	}


	public DescriptionTopic getContent() {
		return content;
	}


	public void setContent(DescriptionTopic content) {
		this.content = content;
	}


	public String getPlatform() {
		return platform;
	}


	public void setPlatform(String platform) {
		this.platform = platform;
	}


	public String getVersion() {
		return version;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public List<String> getEvidenceReference() {
		return evidenceReference;
	}


	public void setEvidenceReference(List<String> evidenceReference) {
		this.evidenceReference = evidenceReference;
	}


	public List<Person> getEvidenceAuthors() {
		return evidenceAuthors;
	}


	public void setEvidenceAuthors(List<Person> evidenceAuthors) {
		this.evidenceAuthors = evidenceAuthors;
	}


	public String getLandingpageUrl() {
		return landingpageUrl;
	}


	public void setLandingpageUrl(String landingpageUrl) {
		this.landingpageUrl = landingpageUrl;
	}
}
