package it.unipd.dei.ims.nanopub.datastructures;

/**
Term element: agent
	URI: http://xmlns.com/foaf/0.1/Agent
	Label: Agent
	Term element: pers
		URI: http://xmlns.com/foaf/0.1/Person
		Label: Pers
		Minimum: 0
		Maximum: 1
		Type of Value: non-literal
		Defined as: person
		Definition: A person.
	Term element: org
		URI: http://xmlns.com/foaf/0.1/Organization
		Label: Org
		Minimum: 0
		Maximum: 1
		Type of Value: non-literal
		Defined as: organisation
		Definition: An organization.*/
public class Agent {
	/**the person representing this agent*/
	public Person pers;
	/**the organization of this agent*/
	public String org;
	
	
	public Person getPers() {
		return pers;
	}
	public void setPers(Person pers) {
		this.pers = pers;
	}
	public String getOrg() {
		return org;
	}
	public void setOrg(String org) {
		this.org = org;
	}
}
