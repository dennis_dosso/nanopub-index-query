package it.unipd.dei.ims.nanopub.datastructures;

import java.util.ArrayList;
import java.util.List;

/**Represents a nanopublication and the information inside of it.
 * It includes new fields added later in the project.
 * 
 * */
public class ExtendedNanopubHolder extends NanopubHolder {

	private String identifier;
	
	private List<EvidenceReference> evidenceReferences;
	
	private String generationComment;
	
	private String rightsHolderId;
	
	/**never used, may be necessary to delete it*/
	private List<ExtendedPerson> authors;
	
	private List<ExtendedPerson> creators;
	
	private String landingPage;
	
	private String creationDate;
	
	private String platform;
	
	private Content content;
	
	private String urlNp;
	
	private String generatedBy;
	
	private String platformVersion;
	
	private String rights;
	
	private String platformId;
	
	private List<ExtendedPerson> collaborators;
	
	private String rightsHolder;
	
	public ExtendedNanopubHolder() {
		this.evidenceReferences = new ArrayList<EvidenceReference>();
		this.creators = new ArrayList<ExtendedPerson>();
		this.authors = new ArrayList<ExtendedPerson>();
		this.collaborators = new ArrayList<ExtendedPerson>();
	}
	
	/**To obtain one string with all the creators written
	 * correctly.
	 * <p>
	 * For now we only include family name and given name.
	 * The other fields appeared to be useless. */
	public String getAuthorsAsOneString() {
		String ret = "";

		for(ExtendedPerson p : authors) {
			ret += p.getFamilyName() + " " 
				 + p.getName() + " ";
		}
		
		return ret;
	}
	
	public String getCreatorsAsOneString() {
		String ret = "";

		for(ExtendedPerson p : this.creators) {
			ret += p.getFamilyName() + " " 
				 + p.getName() + " ";
		}
		
		return ret;
	}
	
	public String getCollaboratorsAsOneString() {
		String ret = "";

		for(ExtendedPerson p : this.collaborators) {
			ret += p.getFamilyName() + " " 
				 + p.getName() + " ";
		}
		
		return ret;
	}
	
	
	public void addToEvidenceReferenes(EvidenceReference ref) {
		this.evidenceReferences.add(ref);
	}
	
	public void addToCreators(ExtendedPerson ext) {
		this.creators.add(ext);
	}
	
	public void addToCollaborators(ExtendedPerson p) {
		this.collaborators.add(p);
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public List<EvidenceReference> getEvidenceReferences() {
		return evidenceReferences;
	}

	public void setEvidenceReferences(List<EvidenceReference> evidenceReferences) {
		this.evidenceReferences = evidenceReferences;
	}

	public String getGenerationComment() {
		return generationComment;
	}

	public void setGenerationComment(String generationComment) {
		this.generationComment = generationComment;
	}

	public String getRightsHolderId() {
		return rightsHolderId;
	}

	public void setRightsHolderId(String rightsHolderId) {
		this.rightsHolderId = rightsHolderId;
	}

	public List<ExtendedPerson> getAuthors() {
		return authors;
	}

	public void setAuthors(List<ExtendedPerson> authors) {
		this.authors = authors;
	}

	public String getLandingPage() {
		return landingPage;
	}

	public void setLandingPage(String landingPage) {
		this.landingPage = landingPage;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}


	public void setContent(Content content) {
		this.content = content;
	}
	
	/**for some strange reason getContent gave compilation
	 * errors, so, yeah, we are stuck with getCont.
	 * Better than nothing I suppose.*/
	public Content getCont() {
		return this.content;
	}

	public String getUrlNp() {
		return urlNp;
	}

	public void setUrlNp(String urlNp) {
		this.urlNp = urlNp;
	}

	public String getGeneratedBy() {
		return generatedBy;
	}

	public void setGeneratedBy(String generatedBy) {
		this.generatedBy = generatedBy;
	}

	public String getPlatformVersion() {
		return platformVersion;
	}

	public void setPlatformVersion(String platformVersion) {
		this.platformVersion = platformVersion;
	}

	public String getRights() {
		return rights;
	}

	public void setRights(String rights) {
		this.rights = rights;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public List<ExtendedPerson> getCollaborators() {
		return collaborators;
	}

	public void setCollaborators(List<ExtendedPerson> collaborators) {
		this.collaborators = collaborators;
	}

	public String getRightsHolder() {
		return rightsHolder;
	}

	public void setRightsHolder(String rightsHolder) {
		this.rightsHolder = rightsHolder;
	}

}
