package it.unipd.dei.ims.nanopub.datastructures;

import java.util.ArrayList;
import java.util.List;

/** Represents an Evidence Reference, an
 * object inside a nanopub */
public class EvidenceReference {
	
	private String nameType;
	/**yeah, this is the abstract of the reference supporting the nanopub*/
	private String abstractText;
	
	private String type;
	/**title of the paper constituting the evidence*/
	private String title;
	/**id of the evidence*/
	private String idEvid; 
	
	private String content;
	
	/**Authors of the paper. They are instances of Person*/
	private List<ExtendedPerson> authors;
	


	public EvidenceReference() {
		authors = new ArrayList<ExtendedPerson>();
	}
	
	
	/**To obtain one string with all the creators written
	 * correctly.
	 * <p>
	 * For now we only include family name and given name.
	 * The other fields appeared to be useless. */
	public String getAuthorsAsOneString() {
		String ret = "";

		for(ExtendedPerson p : authors) {
			ret += p.getFamilyName() + " " 
				 + p.getName() + " ";
		}
		
		return ret;
	}
	
	
	/**Enables you to add one author to the list of authors*/
	public void addToAuthors(ExtendedPerson add) {
		this.authors.add(add);
	}
	
	public String getNameType() {
		return nameType;
	}

	public void setNameType(String nameType) {
		this.nameType = nameType;
	}

	public String getAbstractText() {
		return abstractText;
	}

	public void setAbstractText(String abstractText) {
		this.abstractText = abstractText;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIdEvid() {
		return idEvid;
	}

	public void setIdEvid(String idEvid) {
		this.idEvid = idEvid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<ExtendedPerson> getAuthors() {
		return authors;
	}

	public void setAuthors(List<ExtendedPerson> authors) {
		this.authors = authors;
	}
	
	
}
