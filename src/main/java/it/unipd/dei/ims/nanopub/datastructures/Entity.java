package it.unipd.dei.ims.nanopub.datastructures;

/** Object representing an entity in a nanopublication,
 * usually inside the content
 * */
public class Entity {

	private String idEntity;
	private String entityType;
	private String name;
	private String type;
	private String description;
	
	public String getIdEntity() {
		return idEntity;
	}
	public void setIdEntity(String idEntity) {
		this.idEntity = idEntity;
	}
	public String getEntityType() {
		return entityType;
	}
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
