package it.unipd.dei.ims.indexing;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriter.DocStats;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import it.unipd.dei.ims.nanopub.datastructures.TrecDoc;

/** Indexes the trec files obtained from the nanopubs
 * 
 * 
 * 
 * */
public class IndexNanopubTrecFiles {

	private List<String> fields;

	private String docDelimiter;

	public IndexNanopubTrecFiles() {
		fields = new ArrayList<String>();

		//TODO modify in order to deal with list of fields
		fields.add("DOCNO");
		fields.add("TEXT");
		fields.add("CONTENT");

		this.docDelimiter = "DOC";
	}


	public void indexWithLucene() {
		//path of the directory of the index
		String indexPath = "/Users/dennisdosso/Documents/Ricerca/nanocit/index";
		//path of the directory with the files to index
		String docsPath = "/Users/dennisdosso/Documents/Ricerca/nanocit/docs";

		//TODO definire un modo per inizializzare questa variabile
		//probabilmente devi metterla a falso
		boolean create = true;

		//directory with the files
		Path in = Paths.get(docsPath);

		Date start = new Date();

		//start indexing
		//path representing the output index
		Path out = Paths.get(indexPath);
		try {
			//create the directory for the index
			Directory dir = FSDirectory.open(out);
			//analyzer for the tokenisation
			Analyzer analyzer = new StandardAnalyzer();
			//object representing the configurations of the index
			IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
			
			// here i select the append option. In this way  new documents will be added
			//to the index incrementally
			if (create) {
				// Create a new index in the directory, removing any
				// previously indexed documents:
				iwc.setOpenMode(OpenMode.CREATE);
			} else {
				// Add new documents to an existing index:
				iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
			}

			iwc.setRAMBufferSizeMB(3070.0);

			//writer of the index, with the output directory and the configurations
			IndexWriter writer = new IndexWriter(dir, iwc);

			//method to index the documents
			indexDocs(writer, docsPath);

			writer.commit();

			DocStats dc = writer.getDocStats();
			System.out.println("We indexed: " + dc.numDocs + " documents");
			writer.close();

			Date end = new Date();
			System.out.println(end.getTime() - start.getTime() + " total milliseconds");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Indexes the given file using the given writer, or if a directory is given,
	 * recurses over files and directories found under the given directory.
	 * 
	 * @param writer Writer to the index where the given file/dir info will be stored
	 * @param path The file to index, or the directory to recurse into to find files to index
	 * @throws IOException If there is a low-level I/O error
	 */
	private void indexDocs(final IndexWriter writer, String inPath) throws IOException {
		
		
		Path path = Paths.get(inPath);
		if (Files.isDirectory(path)) {
			//read all the files in the input directory
			Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
				//for every file found
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					try {
						if(!file.toFile().getName().contains(".DS_Store"))
							indexDoc(writer, file, attrs.lastModifiedTime().toMillis());
					} catch (IOException ignore) {
						// don't index files that can't be read.
					}
					return FileVisitResult.CONTINUE;
				}
			});
		} else {
			//it is a single file, not a directory
			if(!path.toFile().getName().contains(".DS_Store"))
				indexDoc(writer, path, Files.getLastModifiedTime(path).toMillis());
		}
	}



	/** Indexes a single trec file (we can have multiple documents inside the file).
	 * 
	 * @param writer the writer that will create the index
	 * @param file the path of the file to be added to the index
	 * 
	 * */
	private void indexDoc(IndexWriter writer, Path file, long lastModified) throws IOException {
		try (InputStream stream = Files.newInputStream(file)) {
			


			//get the path of the file to parse
			String path = file.toFile().getAbsolutePath();
			List<TrecDoc> list = TrecDoc.parse(path, this.fields, this.docDelimiter);

			//now we have the parsing of our documents, we can index them
			for(TrecDoc d : list) {
				//for every document in the file
				//we get the fields and use them to set the Lucene document
				
				// make a new, empty lucene document
				Document doc = new Document();
				
				//DOCNO - it is a single token, so it will be not tokenized
				//using this constructur StringField
				Field id = new StringField("DOCNO", d.getContentOfField("DOCNO"), Field.Store.YES);
				doc.add(id);

				//TEXT
				Field text = new TextField("TEXT", d.getContentOfField("TEXT"), Field.Store.YES);
				doc.add(text);

				//CONTENT
				Field content = new StringField("CONTENT", d.getContentOfField("CONTENT"), Field.Store.YES);
				doc.add(content);

				if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
					// New index, so we just add the document (no old document can be there):
					System.out.println("adding document " +  d.getContentOfField("DOCNO"));
					writer.addDocument(doc);
				} else {
					// Existing index (an old copy of this document may have been indexed) so 
					// we use updateDocument instead to replace the old one matching the exact 
					// path, if present:
					System.out.println("updating document " + d.getContentOfField("DOCNO"));
					writer.updateDocument(new Term("DOCNO", d.getContentOfField("DOCNO")), doc);
				}

			}


		}
	}


	/** Test main in development. Do not use.*/
	public static void main(String[] args) {
		IndexNanopubTrecFiles indexer = new IndexNanopubTrecFiles();
		indexer.indexWithLucene();
	}

}
