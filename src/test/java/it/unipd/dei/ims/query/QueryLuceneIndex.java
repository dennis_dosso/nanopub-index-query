package it.unipd.dei.ims.query;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MultiPhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class QueryLuceneIndex {
	
	public static void queryLuceneIndexTest() {
		String indexPath = "/Users/dennisdosso/Documents/Ricerca/nanocit/index";
		String outputPath = "/Users/dennisdosso/Documents/Ricerca/nanocit";
		
		//putting BM25 as model
		Similarity simfn = new BM25Similarity();
		
		
		try {
			//open the reader on the index
			Path in = Paths.get(indexPath);
			IndexReader reader = DirectoryReader.open(FSDirectory.open(in));
			
			//searcher over an index
			IndexSearcher searcher = new IndexSearcher(reader);
			//set the retrieval model
			searcher.setSimilarity(simfn);
			//analyzer forextracting terms from query
			Analyzer analyzer = new StandardAnalyzer();
			
			MultiPhraseQuery.Builder build = new MultiPhraseQuery.Builder();
			Term term = new Term("TEXT", "wellington");
			build.add(term);
			MultiPhraseQuery q = build.build();
			
//			TopScoreDocCollector collector = TopScoreDocCollector.create(1000, 1000);
//			searcher.search(q, collector);
//			TopDocs td = collector.topDocs();

			TopDocs td = searcher.search(q, 1000);
			ScoreDoc[] docs = td.scoreDocs;
			for(ScoreDoc d : docs) {
				int id = d.doc;
				System.out.println(id);
			}
			
			
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Test where I use a boolean query
	 * @throws ParseException */
	public static void queryTest2() throws IOException, ParseException {
		//open the index
		String indexPath = "/Users/dennisdosso/Documents/Ricerca/nanocit/index";
		Path in = Paths.get(indexPath);
		Directory directory = FSDirectory.open(in);
		DirectoryReader ireader = DirectoryReader.open(directory);
		
		Analyzer analyzer = new StandardAnalyzer();
		IndexReader reader = DirectoryReader.open(FSDirectory.open(in));
		
		//prepare bm25 as model
		Similarity simfn = new BM25Similarity();
		
		QueryParser parser = new QueryParser("TEXT", analyzer);
		
		//searcher over the index
		IndexSearcher searcher = new IndexSearcher(reader);
		searcher.setSimilarity(simfn);

		//prepare the query
//		Query originalQuery = new BooleanQuery.Builder().
//				add(new TermQuery(new Term("TEXT", "prova")), Occur.SHOULD).
//				add(new TermQuery(new Term("DOCNO", "http://rdf.disgenet.org/resource/nanopub/NP887532.RA2SzT2Q5GbcpOKF1d2iZJAUsu0EagaTTPxiZsUSPwa0s")), Occur.MUST).
//				add(new TermQuery(new Term("TEXT", "prova")), Occur.SHOULD).
//				add(new TermQuery(new Term("TEXT", "prova prova")), Occur.SHOULD).
//				add(new TermQuery(new Term("TEXT", "Chang")), Occur.SHOULD).
//				build();
		
		 Query originalQuery = parser.parse("Chang serrano");
		
		
		TopDocs td = searcher.search(originalQuery, 1000);
		ScoreDoc[] hits = td.scoreDocs;
		for(ScoreDoc sd : hits) {
			System.out.println(sd.doc);
			Document hitDoc = searcher.doc(sd.doc);
			System.out.println("DOCNO: " + hitDoc.get("DOCNO"));
		}
		reader.close();
		directory.close();
		System.out.println("done");
		
	}
	
	
	public static void main(String[] args) {
//		QueryLuceneIndex.queryLuceneIndexTest();
		try {
			QueryLuceneIndex.queryTest2();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
