classes to query with lucene

n.b. if you want to use the queryparser, know that it is not present in the core distribution
of lucene. You need to import the package separatedly

In Maven, it's something like this:

<!-- https://mvnrepository.com/artifact/org.apache.lucene/lucene-queryparser -->
<dependency>
    <groupId>org.apache.lucene</groupId>
    <artifactId>lucene-queryparser</artifactId>
    <version>8.2.0</version>
</dependency>
