package it.unipd.dei.ims.nanocit.test.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.unipd.dei.ims.nanopub.query.NanopubQueryManager;


/** An example server where I try to make sense of my 
 * existence and, more in the specific, of how to answer to a 
 * query asking to interrogate an index created with lucene
 * */
public class ServerTest extends Thread {

	private static final Logger logger = Logger.getLogger("ServerTest");

	/** the port of this beautiful server*/
	private int port;

	private boolean keepRunning;

	public ServerTest() {
		this.port = 3475;
		this.keepRunning = true;
	}

	@Override
	public void run() {
		try(ServerSocket server = new ServerSocket(this.port)) {
			logger.info("Accepting connections on port " + server.getLocalPort());
			logger.info("Data to be sent:");
			
			
			//open the class which performs queries
			NanopubQueryManager queryManager = new NanopubQueryManager();
			queryManager.setup();

			while(this.keepRunning) {
				Socket socket = null;
				try {
					//accept the connection
					socket = server.accept();
					//take the channel of communication with the socket
					InputStream din = socket.getInputStream();
					//we use a buffered reader so we can read strings instead of characters (ffs)
					BufferedReader reader = new BufferedReader(new InputStreamReader(din));
					String line = reader.readLine();
					
					line = "https://ww2.sticazzi.org?q=guanta+namera";
					
					//we are making a strong assumption here, that we will receive
					//a single line with the url with the query at the end
					URL url = new URL(line);
					
					//get the query and parse it
					String query = url.getQuery();
					query = query.replaceAll("q=", "").replaceAll("\\+", " ");
					//get our answers
					List<String> answers = queryManager.performOneQuery(query);
					//we write the json with the answers (oh yeah)
					String answer = this.prepareJsonAnswer(answers);
					
					//now prepare to answer back
					OutputStream output = socket.getOutputStream();
					PrintWriter writer = new PrintWriter(output, true);
					writer.println(answer);


				} catch (IOException ex) {
					logger.log(Level.WARNING, "Exception accepting connection", ex);
				} catch (RuntimeException ex) {
					logger.log(Level.SEVERE, "Unexpected error", ex);
				} finally {
					if(socket!=null)
						socket.close();
				}
			}//end while
			
			//close everything related to querying 
			queryManager.shutDown();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Given a list of ids, this method prepares a json in the following 
	 * format and returns it as a string:
	 * <code>
	 * {
	 * 	"response" : [ "id 1", "id 2", "id 3"]
	 * }
	 * </code>
	 * Easy, right?
	 * */
	private String prepareJsonAnswer(List<String> answers) {
		String a = "{\n \"response\": [ ";
		for(String s: answers) {
			a += "\"" + s + "\", ";
		}
		//remove last comma
		a.substring(0, a.length() - 2);
		a+=" ]\n}";
		
		return a;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public boolean isKeepRunning() {
		return keepRunning;
	}

	public void setKeepRunning(boolean keepRunning) {
		this.keepRunning = keepRunning;
	}
}
