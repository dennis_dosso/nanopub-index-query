package it.unipd.dei.ims.nanocit.test.server;

import java.net.MalformedURLException;
import java.net.URL;

/** to test some theories about the 
 * behaviour of the URL object*/
public class UrlTest {

	public static void main(String[] args) throws MalformedURLException {
		String line = "https://ww2.sticazzi.org?q=guanta+namera";
		
		URL url = new URL(line);
		
		String query = url.getQuery();
		query = query.replaceAll("q=", "").replaceAll("\\+", " ");
		
		System.out.println(query);
		
	}
}
